## About

Stirling-PDF is a powerful web based PDF manipulation tool that allows you to perform various operations on PDF files, such as splitting merging, converting, reorganizing, adding images, rotating, compressing, and more. This locally hosted web application started as a 100% ChatGPT-made application and has evolved to include a wide range of features to handle all your PDF needs.
 
## Feature

* Interactive GUI for merging/splitting/rotating/moving PDFs and their pages.
* Split PDFs into multiple files at specified page numbers or extract all pages as individual files.
* Merge multiple PDFs together into a single resultant file
* Convert PDFs to and from images
* Reorganize PDF pages into different orders.
* Add/Generate signatures
* Flatten PDFs
* Repair PDFs
* Detect and remove blank pages
* Compare 2 PDFs and show differences in text
* Add images to PDFs
* Rotating PDFs in 90 degree increments.
* Compressing PDFs to decrease their filesize. (Using OCRMyPDF)
* Add and remove passwords
* Set PDF Permissions
* Add watermark(s)
* Convert Any common file to PDF (using LibreOffice)
* Convert PDF to Word/Powerpoint/Others (using LibreOffice)
* Extract images from PDF
* OCR on PDF (Using OCRMyPDF)
* Edit metadata
* Dark mode support.
* Custom download options (see here for example)
* Parallel file processing and downloads
* API for integration with external scripts
