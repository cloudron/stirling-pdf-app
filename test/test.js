#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const admin_username = "admin";
    const admin_password = "changeme";


    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password, dismissAnalytics = false) {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Sign in"]')).click();

        await browser.sleep(5000);

        if (dismissAnalytics) {
            await waitForElement(By.xpath('//button[text()="Enable analytics"]'));
            await browser.findElement(By.xpath('//button[text()="Enable analytics"]')).click();

            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//span[contains(text(),"settings")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true, dismissAnalytics = false, dismissSurvey = false) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(4000);

        await waitForElement(By.xpath('//a[@data-bs-target="#loginsModal"]'));
        await browser.findElement(By.xpath('//a[@data-bs-target="#loginsModal"]')).click();
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//a[contains(@href, "/oauth2/authorization/")]')).click();
        await browser.sleep(5000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(5000);

        if (dismissAnalytics) {
            await waitForElement(By.xpath('//button[text()="Enable analytics"]'));
            await browser.sleep(2000);
            await browser.findElement(By.xpath('//button[text()="Enable analytics"]')).click();
        }

        await browser.sleep(5000);

        if (dismissSurvey) {
            await waitForElement(By.xpath('//label[@for="dontShowAgain"]'));
            await browser.findElement(By.xpath('//label[@for="dontShowAgain"]')).click();

            await browser.sleep(2000);

            await waitForElement(By.xpath('//div[@id="surveyModal"]//button[text()="Close"]'));
            await browser.findElement(By.xpath('//div[@id="surveyModal"]//button[text()="Close"]')).click();
        }

        await waitForElement(By.xpath('//span[contains(text(),"settings")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await waitForElement(By.xpath('//input[@name="username"]'));
    }

    async function checkWebApp() {
        await browser.get(`https://${app.fqdn}/img-to-pdf`);
        await waitForElement(By.xpath('//label[text()="Colour type"] | //label[text()="Color type"]'));
    }

    async function checkOcr() {
        await browser.get(`https://${app.fqdn}/ocr-pdf`);
        await waitForElement(By.xpath('//button[contains(text(), "Process PDF with OCR")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, admin_username, admin_password, true));
    it('can see the web app', checkWebApp);
    it('can check ocr', checkOcr);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app (SSO)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password, false, true));
    it('can see the web app', checkWebApp);
    it('can check ocr', checkOcr);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can see the web app', checkWebApp);
    it('can logout', logout);

    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can see the web app', checkWebApp);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', async function () {
        execSync('cloudron install --appstore-id stirlingpdf.frooodle.cloudronapp --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password, true, false));
    it('can see the web app', checkWebApp);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can login via OIDC', loginOIDC.bind(null, username, password, true, false, true));
    it('can see the web app', checkWebApp);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
