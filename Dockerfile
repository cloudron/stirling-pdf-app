FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# https://github.com/Frooodle/Stirling-PDF/blob/main/DockerfileBase
# https://github.com/Frooodle/Stirling-PDF/blob/main/LocalRunGuide.md
# NOTE: installing the nogui version crashes on pptx-to-pdf converter
RUN apt-get update && \
    apt-get install -y libleptonica-dev openjdk-17-jdk default-jre libreoffice-core libreoffice-common libreoffice-writer \
        libreoffice-calc libreoffice-impress libreoffice-java-common libreoffice-script-provider-python \
        poppler-utils unpaper qpdf fonts-terminus fonts-dejavu fonts-noto fonts-noto-cjk fonts-font-awesome fonts-noto-extra \
        ttf-mscorefonts-installer fonts-freefont-ttf fonts-liberation unoconv python3-uno pngquant weasyprint && \
    add-apt-repository -y ppa:alex-p/tesseract-ocr5 && \
    apt install -y --no-install-recommends tesseract-ocr tesseract-ocr-{ces,dan,deu,eng,fra,ita,pol,script-taml,spa,tam,nor,swe} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# https://forum.cloudron.io/topic/13343/bad-paths-for-weasyprint-unoconv/10?_=1740729247510
ENV PYTHONPATH=/usr/lib/python3.10:/usr/lib/libreoffice/program:/app/code/site-packages
ENV UNO_PATH=/usr/lib/libreoffice/program
ENV URE_BOOTSTRAP=file:///usr/lib/libreoffice/program/fundamentalrc

RUN mkdir -p /app/code/site-packages && \
    ln -s /usr/lib/libreoffice/program/uno.py /app/code/site-packages/ && \
    ln -s /usr/lib/libreoffice/program/unohelper.py /app/code/site-packages/ && \
    ln -s /usr/lib/libreoffice/program /app/code/site-packages/LibreOffice

RUN pip3 install --no-cache-dir unoserver

# numpy version https://github.com/Frooodle/Stirling-PDF/issues/399 maybe possible to update it in the future to latest
RUN pip3 install --no-cache-dir opencv-python-headless numpy==1.25.2 WeasyPrint pdf2image pillow

# renovate: datasource=github-releases depName=Frooodle/Stirling-PDF versioning=semver extractVersion=^v(?<version>.+)$
ARG STIRLING_VERSION=0.43.1

RUN wget -q https://github.com/Frooodle/Stirling-PDF/releases/download/v$STIRLING_VERSION/Stirling-PDF-with-login.jar && \
    wget -q https://raw.githubusercontent.com/Frooodle/Stirling-PDF/v${STIRLING_VERSION}/src/main/resources/settings.yml.template -O /app/pkg/settings.yml.template

# pipeline/ and scripts/ are required but not part of the jar
RUN curl -L https://github.com/Frooodle/Stirling-PDF/archive/refs/tags/v${STIRLING_VERSION}.tar.gz | tar -xzf - -C /tmp && \
    mv /tmp/Stirling-PDF-${STIRLING_VERSION}/{scripts,pipeline} /app/code/ && \
    rm -rf /tmp/Stirling-PDF-${STIRLING_VERSION}

# onlyoffice needs this path
RUN rm -rf /home/cloudron/.config && ln -s /run/cloudron-dotconfig /home/cloudron/.config && \
    rm -rf /app/code/logs && ln -s /run/logs /app/code/logs && \
    ln -s /app/data/configs /app/code/configs && \
    ln -s /app/data/customFiles /app/code/customFiles && \
    ln -s /app/data/pipeline/watchedFolders /app/code/pipeline/watchedFolders && \
    ln -s /app/data/pipeline/finishedFolders /app/code/pipeline/finishedFolders && \
    rm -rf /home/cloudron/.cache && ln -s /run/cloudron-dotcache /home/cloudron/.cache

# https://github.com/Stirling-Tools/Stirling-PDF/blob/main/HowToUseOCR.md
RUN ln -s /usr/share/tesseract-ocr/5/tessdata /usr/share/tessdata

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/stirling-pdf/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
