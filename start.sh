#!/bin/bash

set -eu

mkdir -p /run/cloudron-dotconfig /run/cloudron-dotcache /app/data/customFiles /app/data/configs /run/logs /run/stirling-pdf
mkdir -p /app/data/pipeline/{watchedFolders,finishedFolders}

if [[ ! -f /app/data/configs/custom_settings.yml ]]; then
    echo "==> Creating configs on first run"
    cp /app/pkg/settings.yml.template /app/data/configs/settings.yml
    yq eval -i ".security.enableLogin=true" /app/data/configs/settings.yml # this can be changed later to disallow auth

    # non-security settings have to go to custom
    touch /app/data/configs/custom_settings.yml
    yq eval -i ".system.customStaticFilePath=\"/app/data/customFiles/static/\"" /app/data/configs/custom_settings.yml

    if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        export SECURITY_INITIALLOGIN_USERNAME="admin"
        export SECURITY_INITIALLOGIN_PASSWORD="changeme"
    fi
fi

yq eval -i ".system.customPaths.operations.weasyprint=\"/usr/bin/weasyprint\"" /app/data/configs/custom_settings.yml
yq eval -i ".system.customPaths.operations.unoconvert=\"/usr/bin/unoconv\"" /app/data/configs/custom_settings.yml

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Configuring OIDC auth"
    yq eval -i ".security.oauth2.enabled=true" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.issuer=\"${CLOUDRON_OIDC_ISSUER}\"" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.clientId=\"${CLOUDRON_OIDC_CLIENT_ID}\"" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.clientSecret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.autoCreateUser=true" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.useAsUsername=\"preferred_username\"" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.scopes=\"openid, email, profile\"" /app/data/configs/settings.yml
    yq eval -i ".security.oauth2.provider=\"${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\"" /app/data/configs/settings.yml
fi

export JAVA_OPTS="-Xmx1024m -Dfile.encoding=UTF-8"

if [[ -f /sys/fs/cgroup/cgroup.controllers ]]; then # cgroup v2
    ram=$(cat /sys/fs/cgroup/memory.max)
    [[ "${ram}" == "max" ]] && ram=$(( 2 * 1024 * 1024 * 1024 )) # "max" means unlimited
else
    ram=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes) # this is the RAM. we have equal amount of swap
fi

if (( ram <= 17179869184 )); then
    ram_mb=$(numfmt --to-unit=1048576 --format "%fm" $ram)
    JAVA_OPTS="${JAVA_OPTS} -Xmx${ram_mb}"
fi

echo "export JAVA_OPTS=\"${JAVA_OPTS}\"" > /run/stirling-pdf/env.sh

chown -R cloudron:cloudron /app/data /run/cloudron-dotcache /run/cloudron-dotconfig /run/logs /run/stirling-pdf

# for some reason, stirling-pdf imports the latest database dump on start up
# https://github.com/Stirling-Tools/Stirling-PDF/discussions/3057
rm -f /app/data/configs/db/backup/*sql

echo "==> Starting Stirling-PDF"
cd /app/code
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i stirling-pdf
